package lab7;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * A graphical user interface of an unit converter.
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015
 *
 */
public class ConverterUI extends JFrame {
	private JButton convertButton, clearButton;
	private JComboBox<Unit> comboBox1, comboBox2;
	private JTextField inputField, resultField;
	private JLabel labelEqual;
	private UnitConverter uc;

	/**
	 * A constructor of converter GUI.
	 * 
	 * @param uc
	 *            is an object unit converter.
	 */
	public ConverterUI(UnitConverter uc) {
		super.setTitle("Length Converter");
		this.uc = uc;
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);

		initialize();
		super.setVisible(true);
	}

	/**
	 * Create a GUI component.
	 */
	public void initialize() {
		Container contents = this.getContentPane();
		contents.setLayout(new FlowLayout());

		// create button
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		// create text field
		inputField = new JTextField(12);
		resultField = new JTextField(12);
		resultField.setEditable(false);
		// create combo box

		Unit[] length = uc.getUnits();

		comboBox1 = new JComboBox<Unit>(length);
		comboBox2 = new JComboBox<Unit>(length);

		// create label
		labelEqual = new JLabel("=");

		contents.add(inputField);
		contents.add(comboBox1);
		contents.add(labelEqual);
		contents.add(resultField);
		contents.add(comboBox2);
		contents.add(convertButton);
		contents.add(clearButton);

		convertButton.addActionListener(new ConvertButtonListener());
		clearButton.addActionListener(new ClearButtonListener());
		inputField.addActionListener(new ConvertButtonListener());
		this.pack();

	}

	/**
	 * 
	 * An action when click or enter the button. It will calculate.
	 */
	class ConvertButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent evt) {
			try {
				resultField.setText(String.valueOf(uc.convert(
						Double.valueOf(inputField.getText()),
						(Unit) comboBox1.getSelectedItem(),
						(Unit) comboBox2.getSelectedItem())));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(getContentPane(),
						"Invalid input.");
			}
		}
	}

	/**
	 * 
	 * A action that perform when click a button. It will clear the text.
	 */
	class ClearButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			inputField.setText("");
			resultField.setText("");
		}

	}
}
