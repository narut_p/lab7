package lab7;

/**
 * A main class.
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015
 */
public class Main {
	public static void main(String[] args) {
		UnitConverter uc = new UnitConverter();
		ConverterUI ui = new ConverterUI(uc);

	}
}
